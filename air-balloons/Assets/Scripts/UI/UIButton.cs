﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
/// <summary>
/// Кнопка на UI
/// </summary>
[RequireComponent(typeof(Button))]
public abstract class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Vector3 reducedSize = new Vector3(0.85f, 0.85f, 0.85f);
    private Button button;
    protected void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Action);
    }

    /// <summary>
    /// Действие на нажатие кнопки
    /// </summary>
    protected abstract void Action();

    public void OnPointerDown(PointerEventData eventData)
    {
        button.transform.localScale = reducedSize;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        button.transform.localScale = new Vector3(1f,1f,1f);
    }
}
