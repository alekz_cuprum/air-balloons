﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Панель счета игрока
/// </summary>
public class PanelPlayerScore : MonoBehaviour
{
    [SerializeField] private Text playerName;
    [SerializeField] private Text score;

    /// <summary>
    /// Установить счет
    /// </summary>
    public void SetScore(string namePlayer, string scorePlayer)
    {
        playerName.text = namePlayer;
        score.text = scorePlayer;
    }
}
