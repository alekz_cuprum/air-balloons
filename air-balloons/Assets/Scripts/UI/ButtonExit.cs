﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Кнопка выхода из игры
/// </summary>
public class ButtonExit : UIButton
{
    protected override void Action()
    {
        Application.Quit();
    }
}
