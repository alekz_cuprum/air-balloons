﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/// <summary>
/// Кнопка старта игры
/// </summary>
public class ButtonStartGame : UIButton
{
    [SerializeField] private string nameLoadScene = "Game";
    protected override void Action()
    {
        if (!string.IsNullOrEmpty(nameLoadScene))
        {
            SceneManager.LoadScene(nameLoadScene);
        }
    }
}
