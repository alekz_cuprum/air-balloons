﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ScorePrefs;
/// <summary>
/// Показ счета
/// </summary>
public class ViewScore : MonoBehaviour
{
    [SerializeField] private GameObject areaSpawn;
    [SerializeField] private GameObject spawnPanel;

    private void Start()
    {
        ScorePrefs.ScorePrefs scorePrefs = FindObjectOfType<ScorePrefs.ScorePrefs>();
        Dictionary<int, ScorePrefs.ScorePrefs.PairData> dictScore = scorePrefs.GetScore();

        if (dictScore.Count > 0)
        {
            spawnPanel.GetComponent<PanelPlayerScore>().SetScore(dictScore[0].NamePlayer, dictScore[0].ScorePlayer.ToString());

            for(int i = 1; i < dictScore.Count; i++)
            {
                GameObject obj = Instantiate(spawnPanel);
                obj.transform.SetParent(areaSpawn.transform);
                obj.GetComponent<PanelPlayerScore>().SetScore(dictScore[i].NamePlayer, dictScore[i].ScorePlayer.ToString());
            }
        }

    }
}
