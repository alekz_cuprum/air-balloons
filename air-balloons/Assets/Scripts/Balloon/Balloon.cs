﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/// <summary>
/// Класс воздушного шарика
/// </summary>
public class Balloon : MonoBehaviour
{
    [SerializeField] private float maxHeight = 16f;
    [SerializeField] private float leftSide = -7f;
    [SerializeField] private float rightSide = 7f;

    [Space]
    [SerializeField] private float minSpeed = 1f;
    [SerializeField] private float maxSpeed = 10f;

    [Space]
    [SerializeField] private MeshRenderer[] meshRenderers;

    private ScoreController scoreController;
    private GameController gameController;
    private Coroutine moveCoroutine = null;

    public Action onDeath = delegate { };

    private void Start()
    {
        scoreController = FindObjectOfType<ScoreController>();
        gameController = FindObjectOfType<GameController>();

        moveCoroutine = StartCoroutine(Move());
    }

    /// <summary>
    /// Установить материал у шарика
    /// </summary>
    public void SetMaterial(Material newMaterial)
    {
        foreach (var renderer in meshRenderers)
        {
            renderer.material = newMaterial;
        }
    }

    private IEnumerator Move()
    {
        float speed = UnityEngine.Random.Range(minSpeed, maxSpeed);
        while(transform.position.y < maxHeight)
        {
            transform.position += new Vector3(0f, speed * Time.deltaTime, 0f);
            yield return new WaitForEndOfFrame();

        }

        Death();
    }

    /// <summary>
    /// Смерть от высоты
    /// </summary>
    public void Death()
    {
        gameController.MinusHealth();

        DestroyBalloon();
    }

    /// <summary>
    /// Попадание в шарик
    /// </summary>
    public void Hit()
    {
        scoreController.UpdateScore();
        DestroyBalloon();
    }

    private void DestroyBalloon()
    {
        StopCoroutine(moveCoroutine);
        moveCoroutine = null;

        Destroy(this.gameObject);
    }
}
