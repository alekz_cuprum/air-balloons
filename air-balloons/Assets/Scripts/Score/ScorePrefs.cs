﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Linq.Expressions;
namespace ScorePrefs
{

    /// <summary>
    /// Управление сохраненным счетом
    /// </summary>
    public class ScorePrefs : MonoBehaviour
    {
        /// <summary>
        /// Пара игрок-счет
        /// </summary>
        public class PairData
        {
            public string NamePlayer;
            public int ScorePlayer;

            public PairData(string name, int score)
            {
                NamePlayer = name;
                ScorePlayer = score;
            }
        }

        private const string KEY_NAME_PLAYER = "Player_";
        private const string KEY_SCORE_PLAYER = "Score_";

        public void SavePrefs(string namePlayer, int scorePlayer)
        {
            Dictionary<int, PairData> prefs = new Dictionary<int, PairData>();
            PairData pairData;
            int i;
            for (i = 0; ; i++)
            {
                if (PlayerPrefs.HasKey(KEY_NAME_PLAYER + i.ToString()))
                {
                    pairData = new PairData(PlayerPrefs.GetString(KEY_NAME_PLAYER + i.ToString()), PlayerPrefs.GetInt(KEY_SCORE_PLAYER + i.ToString()));
                    prefs.Add(i, pairData);
                }
                else
                {
                    break;
                }
            }

            pairData = new PairData(namePlayer, scorePlayer);
            prefs.Add(i, pairData);

            List<PairData> list = prefs.Values.ToList();
            list.Sort(delegate(PairData x, PairData y)
            {
                if (x.ScorePlayer < y.ScorePlayer) return 1;
                else if (x.ScorePlayer > y.ScorePlayer) return -1;
                else return 0;
            }
            );

            for(i = 0; i < prefs.Count; i++)
            {
                PlayerPrefs.SetString(KEY_NAME_PLAYER + i.ToString(), list[i].NamePlayer);
                PlayerPrefs.SetInt(KEY_SCORE_PLAYER + i.ToString(), list[i].ScorePlayer);
            }
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Получение счета
        /// </summary>
        public Dictionary<int, PairData> GetScore()
        {
            Dictionary<int, PairData> score = new Dictionary<int, PairData>();
            PairData pairData;

            for (int i = 0; ; i++)
            {
                if (PlayerPrefs.HasKey(KEY_NAME_PLAYER + i.ToString()))
                {
                    pairData = new PairData(PlayerPrefs.GetString(KEY_NAME_PLAYER + i.ToString()), PlayerPrefs.GetInt(KEY_SCORE_PLAYER + i.ToString()));
                    score.Add(i, pairData);
                }
                else
                {
                    break;
                }
            }
            return score;
        }
    }
}