﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputGame : MonoBehaviour
{
    private RaycastHit hit;
    private Ray ray;
    private Balloon balloon = null;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Select();
        }
    }

    private void Select()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit))
        {
            balloon = hit.collider.gameObject.GetComponent<Balloon>();
            if (balloon != null)
            {
                balloon.Hit();
            }
        }
    }
}
