﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/// <summary>
/// Управление игрой
/// </summary>
public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject[] healthIcons = new GameObject[3];

    [Space]
    [SerializeField] private GameObject panelGameOver;
    [SerializeField] private InputField inputName;
    [SerializeField] private Text textScore;

    private int health;

    private ScorePrefs.ScorePrefs scorePrefs;
    private ScoreController scoreController;
    private InputGame inputGame;
    private const string NAME_LATE_RECORD = "LateName";
    private const string GAMEOVER_SCORE = "Score\n";

    private void Start()
    {
        Time.timeScale = 1f;
        health = 3;
        scorePrefs = FindObjectOfType<ScorePrefs.ScorePrefs>();
        scoreController = FindObjectOfType<ScoreController>();
        inputGame = FindObjectOfType<InputGame>();
    }

    /// <summary>
    /// Уменьшение жизни
    /// </summary>
    public void MinusHealth()
    {
        health--;
        healthIcons[health].SetActive(false);
        
        if(health == 0)
        {
            GameOver();
        }

    }

    private void GameOver()
    {
        Time.timeScale = 0f;

        inputGame.gameObject.SetActive(false);
        panelGameOver.SetActive(true);
        textScore.text = GAMEOVER_SCORE + scoreController.Score;

        if (!PlayerPrefs.HasKey(NAME_LATE_RECORD))
        {
            inputName.ActivateInputField();
        }
        else
        {
            inputName.text = PlayerPrefs.GetString(NAME_LATE_RECORD);
        }
    }

    /// <summary>
    /// Сохранить рекорд
    /// </summary>
    public void SetNewRecord()
    {
        PlayerPrefs.SetString(NAME_LATE_RECORD, inputName.text);
        scorePrefs.SavePrefs(inputName.text, scoreController.Score);
    }

    /// <summary>
    /// Загрузка сцены
    /// </summary>
    /// <param name="nameScene"></param>
    public void LoadScene(string nameScene)
    {
        SceneManager.LoadScene(nameScene);
    }
}
