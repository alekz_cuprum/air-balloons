﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Управление спавном воздушных шариков
/// </summary>
public class SpawnBalloons : MonoBehaviour
{
    [SerializeField] private Object prefab;
    [SerializeField] private GameObject spawnArea;

    [Space]
    [SerializeField] private float minHeight = -16f;
    [SerializeField] private float leftSide = -7f;
    [SerializeField] private float rightSide = 7f;
    [SerializeField] private float minDepth = 0f;
    [SerializeField] private float maxDepth = 10f;

    [Space]
    [SerializeField] private float minTimeSpawn = 0.1f;
    [SerializeField] private float maxTimeSpawn = 2f;

    [Space]
    [SerializeField] private Material[] balloonsMaterials;
    private Coroutine spawnCorounite = null;

    private void Start()
    {
        spawnCorounite = StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while(true)
        {
            GameObject obj = Instantiate(prefab) as GameObject;
            obj.transform.parent = spawnArea.transform;
            obj.transform.localPosition = new Vector3(Random.Range(leftSide, rightSide), minHeight, Random.Range(minDepth,maxDepth));
            obj.GetComponent<Balloon>().SetMaterial(balloonsMaterials[Random.Range(0, balloonsMaterials.Length)]);


            yield return new WaitForSeconds(Random.Range(minTimeSpawn,maxTimeSpawn));  

        }
    }

    private void OnDestroy()
    {
        StopCoroutine(spawnCorounite);
        spawnCorounite = null;
    }
}
