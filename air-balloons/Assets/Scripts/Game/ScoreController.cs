﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Управление счетом на игровом экране
/// </summary>
public class ScoreController : MonoBehaviour
{
    [SerializeField] private Text textScore;

    private int score;
    /// <summary>
    /// Игровой счет
    /// </summary>
    public int Score => score;

    private const string TEXT_SCORE = "Score: ";

    private void Start()
    {
        score = 0;
        UpdateText();
    }

    public void UpdateScore()
    {
        score++;
        UpdateText();
    }

    private void UpdateText()
    {
        textScore.text = TEXT_SCORE + score.ToString();
    }
}
